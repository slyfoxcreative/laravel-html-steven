<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

use SlyFoxCreative\Html\FormModelStack;

use function SlyFoxCreative\Html\Helpers\attributeString;
use function SlyFoxCreative\Html\Helpers\attributeValue;
use function SlyFoxCreative\Html\Helpers\makeId;
use function SlyFoxCreative\Html\Helpers\oldValue;

class HelpersTest extends TestCase
{
    public function testAttributeValue(): void
    {
        // TODO: Remove this after TValue on Illuminate\Support\Collection
        // is made covariant.
        /** @var AttributeValueCollection */
        $values = collect([
            0,
            '0',
            'value2',
            'value1' => true,
            'value3' => false,
            'value4' => null,
        ]);

        self::assertSame('0 0 value1 value2', attributeValue($values));
    }

    public function testAttributeString(): void
    {
        // TODO: Remove this after TValue on Illuminate\Support\Collection
        // is made covariant.
        /** @var AttributeCollection */
        $attributes = collect([
            'custom' => null,
            'disabled' => true,
            'hidden' => false,
            'value' => 1,
            'name' => 'test',
            'href' => '',
            'class' => ['a', 'b'],
            'rel' => collect(['a', 'b']),
            'data' => [
                'test1' => 1,
                'test2' => [],
                'test3' => collect(),
            ],
            'aria' => [
                'label' => 'test',
            ],
        ]);

        self::assertSame(
            "aria-label='test' class='a b' data-test1='1' disabled name='test' rel='a b' value='1'",
            attributeString($attributes),
        );
    }

    public function testMakeId(): void
    {
        self::assertSame('things_test_1', makeId('things[test][1]'));
    }

    public function testOldValueWithNoFlashedInput(): void
    {
        self::assertNull(oldValue('test'));
    }

    public function testOldValueWithFlashedInput(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertSame('value', oldValue('test'));
    }

    public function testOldValueWithDefault(): void
    {
        self::assertSame('defaultvalue', oldValue('test', 'defaultvalue'));
    }

    public function testOldValueWithFlashedInputAndDefault(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertSame('value', oldValue('test', 'defaultvalue'));
    }

    public function testOldValueWithNestedFlashedInput(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => ['nested' => 'value']]);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertSame('value', oldValue('test.nested'));
    }

    public function testOldValueWithModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'modelvalue']);

        self::assertSame('modelvalue', oldValue('test'));
    }

    public function testOldValueWithEloquentModel(): void
    {
        resolve(FormModelStack::class)->push(TestModel::create(['test' => 'test']));

        self::assertSame('test', oldValue('test'));
    }

    public function testOldValueWithNonexistentPropertyOnEloquentModel(): void
    {
        TestModel::create();
        resolve(FormModelStack::class)->push(TestModel::firstOrFail());

        self::assertNull(oldValue('nonexistent'));
    }

    public function testOldValueWithFlashedInputAndModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'modelvalue']);

        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertSame('value', oldValue('test'));
    }

    public function testOldValueWithDefaultAndModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'modelvalue']);

        self::assertSame('modelvalue', oldValue('test', 'defaultvalue'));
    }

    public function testOldValueWithFlashedInputAndDefaultAndModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'modelvalue']);

        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertSame('value', oldValue('test', 'defaultvalue'));
    }

    public function testOldValueWithModelRelationship(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => TestModel::create()]);

        self::assertNull(oldValue('test'));
    }
}
