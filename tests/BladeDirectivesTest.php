<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

class BladeDirectivesTest extends TestCase
{
    public function testModel(): void
    {
        $blade = '@model($user)';

        $expected = "<?php resolve('SlyFoxCreative\\Html\\FormModelStack')->push(\$user); ?>";

        self::assertSame($expected, app('blade.compiler')->compileString($blade));
    }

    public function testEndmodel(): void
    {
        $blade = '@endmodel';

        $expected = "<?php resolve('SlyFoxCreative\\Html\\FormModelStack')->pop(); ?>";

        self::assertSame($expected, app('blade.compiler')->compileString($blade));
    }

    public function testMarkdown(): void
    {
        $blade = "@markdown('This is a test.')";

        $expected = "<?php echo SlyFoxCreative\\Html\\markdown('This is a test.'); ?>";

        self::assertSame($expected, app('blade.compiler')->compileString($blade));
    }

    public function testInputDirectives(): void
    {
        collect([
            'checkbox',
            'date',
            'datetime_local',
            'email',
            'file',
            'hidden',
            'month',
            'number',
            'password',
            'radio',
            'range',
            'reset',
            'search',
            'submit',
            'tel',
            'text',
            'time',
            'url',
            'week',
            'select',
            'textarea',
        ])->each(function ($name) {
            $blade = "@{$name}(['attribute' => 'value'])";

            $expected = "<?php echo SlyFoxCreative\\Html\\{$name}(['attribute' => 'value']); ?>";

            self::assertSame($expected, app('blade.compiler')->compileString($blade));
        });
    }

    public function testLabelDirective(): void
    {
        $blade = "@label('test')";

        $expected = <<<'blade'
                <?php
                    if (isset($component) && method_exists($component, 'label')) {
                        echo $component->label('test');
                    } else {
                        echo SlyFoxCreative\Html\label('test');
                    }
                ?>
            blade;

        self::assertSame($expected, app('blade.compiler')->compileString($blade));
    }
}
