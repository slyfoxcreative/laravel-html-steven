<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests\Components;

use SlyFoxCreative\Html\Components\Form;
use SlyFoxCreative\Html\FormModelStack;
use SlyFoxCreative\Html\Tests\TestCase;

class FormTest extends TestCase
{
    public function testDefaultAttributeValues(): void
    {
        $form = new Form();

        self::assertSame('get', $form->method);
        self::assertSame('get', $form->realMethod);
        self::assertNull($form->model);
        self::assertSame(['method' => 'get'], $form->additionalAttributes());
    }

    public function testAttributes(): void
    {
        $object = new \stdClass();
        $form = new Form('post', true, 'remote', $object);

        $expectedAttributes = [
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            'data-controller' => 'remote',
            'data-target' => 'remote.form',
            'data-action' => 'remote#submit',
        ];

        self::assertSame('post', $form->method);
        self::assertSame('post', $form->realMethod);
        self::assertSame($object, $form->model);
        self::assertSame($object, resolve(FormModelStack::class)->last());
        self::assertSame($expectedAttributes, $form->additionalAttributes());
    }

    public function testMethodAttributeWithNonstandardValue(): void
    {
        $form = new Form('put');

        self::assertSame('post', $form->method);
        self::assertSame('put', $form->realMethod);
        self::assertSame(['method' => 'post'], $form->additionalAttributes());
    }

    public function testRender(): void
    {
        $form = new Form();

        self::assertSame('html::form', $form->render()->name());
    }
}
