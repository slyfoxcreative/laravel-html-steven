<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

use Illuminate\Support\HtmlString;

use function SlyFoxCreative\Html\closing_tag;
use function SlyFoxCreative\Html\complete_tag;
use function SlyFoxCreative\Html\opening_tag;

class HtmlTest extends TestCase
{
    public function testOpeningTag(): void
    {
        // TODO: Remove this after TValue on Illuminate\Support\Collection
        // is made covariant.
        /** @var AttributeCollection */
        $attributes = collect(['class' => ['test']]);

        self::assertEquals(
            new HtmlString("<div class='test'>"),
            opening_tag('div', $attributes),
        );
    }

    public function testClosingTag(): void
    {
        self::assertEquals(new HtmlString('</div>'), closing_tag('div'));
    }

    public function testCompleteTag(): void
    {
        // TODO: Remove this after TValue on Illuminate\Support\Collection
        // is made covariant.
        /** @var AttributeCollection */
        $attributes = collect(['class' => ['test']]);

        self::assertEquals(
            new HtmlString("<div class='test'>Test</div>"),
            complete_tag('div', 'Test', $attributes),
        );
    }
}
