<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

use Illuminate\Support\HtmlString;
use SlyFoxCreative\Html\FormModelStack;

use function SlyFoxCreative\Html\checkbox;
use function SlyFoxCreative\Html\date;
use function SlyFoxCreative\Html\datetime_local;
use function SlyFoxCreative\Html\email;
use function SlyFoxCreative\Html\file;
use function SlyFoxCreative\Html\hidden;
use function SlyFoxCreative\Html\input;
use function SlyFoxCreative\Html\label;
use function SlyFoxCreative\Html\month;
use function SlyFoxCreative\Html\number;
use function SlyFoxCreative\Html\password;
use function SlyFoxCreative\Html\radio;
use function SlyFoxCreative\Html\range;
use function SlyFoxCreative\Html\reset;
use function SlyFoxCreative\Html\search;
use function SlyFoxCreative\Html\select;
use function SlyFoxCreative\Html\submit;
use function SlyFoxCreative\Html\tel;
use function SlyFoxCreative\Html\text;
use function SlyFoxCreative\Html\textarea;
use function SlyFoxCreative\Html\time;
use function SlyFoxCreative\Html\url;
use function SlyFoxCreative\Html\week;

class FormsTest extends TestCase
{
    public function testLabel(): void
    {
        self::assertEquals(
            new HtmlString("<label class='test' for='test'>Test</label>"),
            label('test', 'Test', ['class' => ['test']]),
        );
    }

    public function testLabelWithArray(): void
    {
        self::assertEquals(
            new HtmlString("<label class='test' for='test_thing'>Test thing</label>"),
            label('test_thing', attributes: ['class' => 'test']),
        );
    }

    public function testLabelWithCollection(): void
    {
        // TODO: Remove this after TValue on Illuminate\Support\Collection
        // is made covariant.
        /** @var AttributeCollection */
        $attributes = collect(['class' => 'test']);

        self::assertEquals(
            new HtmlString("<label class='test' for='test_thing'>Test thing</label>"),
            label('test_thing', attributes: $attributes),
        );
    }

    public function testLabelWithIdSuffix(): void
    {
        self::assertEquals(
            new HtmlString("<label for='test_id'>Test</label>"),
            label('test_id'),
        );
    }

    public function testSelect(): void
    {
        $expected = <<<'EOS'
            <select id='test' name='test'>
            <option selected value='a'>value 1</option>
            <option value='b'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select('test', ['a' => 'value 1', 'b' => 'value 2'], ['selected' => 'a']),
        );
    }

    public function testSelectWithOptgroups(): void
    {
        $expected = <<<'EOS'
            <select id='test' name='test'>
            <optgroup label='Test 1'>
            <option selected value='a'>value 1</option>
            </optgroup>
            <optgroup label='Test 2'>
            <option value='b'>value 2</option>
            </optgroup>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        $options = [
            'Test 1' => [
                'a' => 'value 1',
            ],
            'Test 2' => [
                'b' => 'value 2',
            ],
        ];

        self::assertEquals(
            $expected,
            select('test', $options, ['selected' => 'a']),
        );
    }

    public function testSelectWithOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'a']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        $expected = <<<'EOS'
            <select id='test' name='test'>
            <option selected value='a'>value 1</option>
            <option value='b'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select('test', ['a' => 'value 1', 'b' => 'value 2']),
        );
    }

    public function testSelectWithNumericOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'a']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        $expected = <<<'EOS'
            <select id='test' name='test'>
            <option selected value='a'>value 1</option>
            <option value='b'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select('test', ['a' => 'value 1', 'b' => 'value 2']),
        );
    }

    public function testSelectWithModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => '1']);

        $expected = <<<'EOS'
            <select id='test' name='test'>
            <option selected value='1'>value 1</option>
            <option value='2'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select('test', [1 => 'value 1', 2 => 'value 2']),
        );
    }

    public function testSelectOldValueDoesNotOverrideAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'a']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        $expected = <<<'EOS'
            <select id='test' name='test'>
            <option value='a'>value 1</option>
            <option selected value='b'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select('test', ['a' => 'value 1', 'b' => 'value 2'], ['selected' => 'b']),
        );
    }

    public function testTextarea(): void
    {
        self::assertEquals(
            new HtmlString("<textarea class='test' id='test' name='test'>Test</textarea>"),
            textarea('test', 'Test', ['class' => ['test']]),
        );
    }

    public function testTextareaWithNoText(): void
    {
        self::assertEquals(
            new HtmlString("<textarea id='test' name='test'></textarea>"),
            textarea('test'),
        );
    }

    public function testTextareaWithOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'old value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<textarea id='test' name='test'>old value</textarea>"),
            textarea('test'),
        );
    }

    public function testTextareaWithModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'old value']);

        self::assertEquals(
            new HtmlString("<textarea id='test' name='test'>old value</textarea>"),
            textarea('test'),
        );
    }

    public function testTextareaOldValueDoesNotOverrideText(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'old value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<textarea id='test' name='test'>new value</textarea>"),
            textarea('test', 'new value'),
        );
    }

    public function testTextareaWithNoTextAndAttributes(): void
    {
        self::assertEquals(
            new HtmlString("<textarea class='test' id='test' name='test'></textarea>"),
            textarea('test', attributes: ['class' => ['test']]),
        );
    }

    public function testTextareaWithValueAttribute(): void
    {
        self::assertEquals(
            new HtmlString("<textarea id='test' name='test'>test</textarea>"),
            textarea('test', attributes: ['value' => 'test']),
        );
    }

    public function testTextareaOldValueDoesNotOverrideValueAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'old value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<textarea id='test' name='test'>new value</textarea>"),
            textarea('test', attributes: ['value' => 'new value']),
        );
    }

    public function testInputWithOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'old value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input id='test' name='test' value='old value'>"),
            input('test', collect()),
        );
    }

    public function testInputWithModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'old value']);

        self::assertEquals(
            new HtmlString("<input id='test' name='test' value='old value'>"),
            input('test', collect()),
        );
    }

    public function testInputOldValueDoesNotOverrideAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'old value']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        // TODO: Remove this after TValue on Illuminate\Support\Collection
        // is made covariant.
        /** @var AttributeCollection */
        $attributes = collect(['value' => 'new value']);

        self::assertEquals(
            new HtmlString("<input id='test' name='test' value='new value'>"),
            input('test', $attributes),
        );
    }

    public function testCheckbox(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test_test' name='test' type='checkbox' value='test'>"),
            checkbox('test', ['value' => 'test']),
        );
    }

    public function testCheckboxWithOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'test']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input checked id='test_test' name='test' type='checkbox' value='test'>"),
            checkbox('test', ['value' => 'test']),
        );
    }

    public function testCheckboxWithNumericOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => '1']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input checked id='test_1' name='test' type='checkbox' value='1'>"),
            checkbox('test', ['value' => 1]),
        );
    }

    public function testCheckboxWithOldValueAndNoValueAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => '1']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input checked id='test' name='test' type='checkbox' value='1'>"),
            checkbox('test'),
        );
    }

    public function testCheckboxWithModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'test']);

        self::assertEquals(
            new HtmlString("<input checked id='test_test' name='test' type='checkbox' value='test'>"),
            checkbox('test', ['value' => 'test']),
        );
    }

    public function testCheckboxWithModelAndBooleanAttribute(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => true]);

        self::assertEquals(
            new HtmlString("<input checked id='test' name='test' type='checkbox' value='1'>"),
            checkbox('test'),
        );
    }

    public function testCheckboxOldValueDoesNotOverrideAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'test']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input id='test_test' name='test' type='checkbox' value='test'>"),
            checkbox('test', ['value' => 'test', 'checked' => false]),
        );
    }

    public function testDate(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='date' value='2019-01-01'>"),
            date('test', ['value' => '2019-01-01']),
        );
    }

    public function testDateWithDateTimeModelValue(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => new \DateTime('2019-01-01')]);

        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='date' value='2019-01-01'>"),
            date('test'),
        );
    }

    public function testDatetimeLocal(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='datetime-local' value='2019-01-01T00:00'>"),
            datetime_local('test', ['value' => '2019-01-01T00:00']),
        );
    }

    public function testEmail(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='email' value='test@example.com'>"),
            email('test', ['value' => 'test@example.com']),
        );
    }

    public function testFile(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='file' value='test.pdf'>"),
            file('test', ['value' => 'test.pdf']),
        );
    }

    public function testHidden(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='hidden' value='test'>"),
            hidden('test', ['value' => 'test']),
        );
    }

    public function testMonth(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='month' value='2019-01'>"),
            month('test', ['value' => '2019-01']),
        );
    }

    public function testNumber(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='number' value='1'>"),
            number('test', ['value' => '1']),
        );
    }

    public function testPassword(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='password' value='test'>"),
            password('test', ['value' => 'test']),
        );
    }

    public function testRadio(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test_test' name='test' type='radio' value='test'>"),
            radio('test', ['value' => 'test']),
        );
    }

    public function testRadioWithOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'test']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input checked id='test_test' name='test' type='radio' value='test'>"),
            radio('test', ['value' => 'test']),
        );
    }

    public function testRadioWithNumericOldValue(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => '1']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input checked id='test_1' name='test' type='radio' value='1'>"),
            radio('test', ['value' => 1]),
        );
    }

    public function testRadioWithOldValueAndNoValueAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'on']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input checked id='test' name='test' type='radio' value='on'>"),
            radio('test'),
        );
    }

    public function testRadioWithModel(): void
    {
        resolve(FormModelStack::class)->push((object) ['test' => 'test']);

        self::assertEquals(
            new HtmlString("<input checked id='test_test' name='test' type='radio' value='test'>"),
            radio('test', ['value' => 'test']),
        );
    }

    public function testRadioOldValueDoesNotOverrideAttribute(): void
    {
        app('router')->get('test', ['middleware' => 'web', 'uses' => function () {
            $request = request()->merge(['test' => 'test']);
            $request->flash();
        }]);

        $this->call('GET', 'test');

        self::assertEquals(
            new HtmlString("<input id='test_test' name='test' type='radio' value='test'>"),
            radio('test', ['value' => 'test', 'checked' => false]),
        );
    }

    public function testRange(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='range' value='1'>"),
            range('test', ['value' => '1']),
        );
    }

    public function testReset(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='reset' value='Reset'>"),
            reset('Reset', ['name' => 'test']),
        );
    }

    public function testSearch(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='search' value='test'>"),
            search('test', ['value' => 'test']),
        );
    }

    public function testSubmit(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='submit' value='Submit'>"),
            submit('Submit', ['name' => 'test']),
        );
    }

    public function testTel(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='tel' value='123-123-1234'>"),
            tel('test', ['value' => '123-123-1234']),
        );
    }

    public function testText(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='text' value='test'>"),
            text('test', ['value' => 'test']),
        );
    }

    public function testTime(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='time' value='00:00'>"),
            time('test', ['value' => '00:00']),
        );
    }

    public function testUrl(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='url' value='http://example.com'>"),
            url('test', ['value' => 'http://example.com']),
        );
    }

    public function testWeek(): void
    {
        self::assertEquals(
            new HtmlString("<input id='test' name='test' type='week' value='2019-W01'>"),
            week('test', ['value' => '2019-W01']),
        );
    }
}
