<form {{ $attributes->merge($additionalAttributes()) }}>
  @if ($realMethod !== $method)
    @method($realMethod)
  @endif
  @if ($method === 'post')
    @csrf
  @endif
  {{ $slot }}
</form>
@if (!is_null($model))
  @php
    resolve(\SlyFoxCreative\Html\FormModelStack::class)->pop();
  @endphp
@endif
