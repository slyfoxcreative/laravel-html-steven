<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use SlyFoxCreative\Html\FormModelStack;

/**
 * HTML form component.
 *
 * Special attributes:
 *  - method: values other than "get" and "post" that are not supported by HTML
 *            are handled automatically
 *  - file: whether to allow file uploads
 *  - controller: Stimulus controller name
 *  - model: an object to use for filling initial input values
 */
class Form extends Component
{
    public string $method;

    public string $realMethod;

    public ?object $model;

    private bool $file;

    private ?string $controller;

    public function __construct(
        string $method = 'get',
        bool $file = false,
        ?string $controller = null,
        ?object $model = null,
    ) {
        $this->file = $file;
        $this->controller = $controller;
        $this->realMethod = $method;
        $this->method = in_array($this->realMethod, ['get', 'post'], strict: true) ? $this->realMethod : 'post';
        $this->model = $model;

        if (! is_null($this->model)) {
            resolve(FormModelStack::class)->push($this->model);
        }
    }

    /**
     * Get additional attributes to be merged into the user-supplied
     * attributes.
     *
     * @return array<string, string>
     */
    public function additionalAttributes(): array
    {
        $attributes = [
            'method' => $this->method,
        ];
        if ($this->file) {
            $attributes['enctype'] = 'multipart/form-data';
        }
        if (! is_null($this->controller)) {
            $attributes['data-controller'] = "{$this->controller}";
            $attributes['data-target'] = "{$this->controller}.form";
            $attributes['data-action'] = "{$this->controller}#submit";
        }

        return $attributes;
    }

    public function render(): View
    {
        return view('html::form');
    }
}
