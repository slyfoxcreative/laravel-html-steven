<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\Facades\Blade;
use League\CommonMark\CommonMarkConverter;
use SlyFoxCreative\Html\Components\Form;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

use function SlyFoxCreative\Utilities\assert_array;

class ServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-html')
            ->hasConfigFile()
            ->hasViews()
            ->hasViewComponents('html', Form::class)
        ;
    }

    public function packageRegistered(): void
    {
        $this->app->singleton(CommonMarkConverter::class, function ($app) {
            $config = config('html.markdown');
            assert_array($config);
            return new CommonMarkConverter($config);
        });

        $this->app->singleton(
            AttributeModifierRegistry::class,
            fn($app) => new AttributeModifierRegistry(),
        );

        $this->app->singleton(FormModelStack::class, fn($app) => new FormModelStack());
    }

    public function packageBooted(): void
    {
        Blade::directive('model', function ($expression) {
            return "<?php resolve('SlyFoxCreative\\Html\\FormModelStack')->push({$expression}); ?>";
        });

        Blade::directive('endmodel', function () {
            return "<?php resolve('SlyFoxCreative\\Html\\FormModelStack')->pop(); ?>";
        });

        Blade::directive('markdown', function ($expression) {
            return "<?php echo SlyFoxCreative\\Html\\markdown({$expression}); ?>";
        });

        collect([
            'checkbox',
            'date',
            'datetime_local',
            'email',
            'file',
            'hidden',
            'month',
            'number',
            'password',
            'radio',
            'range',
            'reset',
            'search',
            'submit',
            'tel',
            'text',
            'time',
            'url',
            'week',
            'select',
            'option',
            'textarea',
        ])->each(function ($name) {
            Blade::directive($name, fn($e) => "<?php echo SlyFoxCreative\\Html\\{$name}({$e}); ?>");
        });

        Blade::directive('label', function ($expression) {
            return <<<"blade"
                    <?php
                        if (isset(\$component) && method_exists(\$component, 'label')) {
                            echo \$component->label({$expression});
                        } else {
                            echo SlyFoxCreative\\Html\\label({$expression});
                        }
                    ?>
                blade;
        });
    }
}
