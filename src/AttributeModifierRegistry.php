<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\Collection;

/**
 * A registry of attribute-modifier functions.
 */
class AttributeModifierRegistry
{
    /**
     * The Collection of registered modifiers.
     *
     * @var Collection<int, AttributeModifier>
     */
    private Collection $modifiers;

    public function __construct()
    {
        $this->modifiers = collect();
    }

    /**
     * Add a modifier to the registry.
     *
     * @param  callable  $modifier  the modifier function
     * @param  array<int, string>  $tags  the list of tags that the modifier function applies to
     */
    public function add(callable $modifier, array $tags = []): void
    {
        $this->modifiers->push(new AttributeModifier($modifier, $tags));
    }

    /**
     * Modify a Collection of attributes.
     *
     * The modifier functions are applied in the order they were registered.
     *
     * @param  AttributeCollection  $attributes  the Collection of attributes to modify
     * @param  string  $tag  apply modifiers tagged with this tag
     * @param  array<string, mixed>  $context  optional array of context values to be used by the
     *                                         modifier functions
     * @return AttributeCollection
     */
    public function modify(Collection $attributes, string $tag, array $context = []): Collection
    {
        return $this->modifiers
            ->filter->appliesToTag($tag)
            ->reduce(
                fn($attributes, $m) => $m->apply($attributes, $context),
                $attributes,
            )
        ;
    }
}
