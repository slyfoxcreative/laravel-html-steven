<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SlyFoxCreative\Html\FormModelStack;

use function SlyFoxCreative\Utilities\is_instance_of;

/**
 * Return HTML attribute value generated from a Collection of values.
 *
 * attributeValue(collect([
 *   'class1',           // Item value included unconditionally
 *   'class2' => true,   // Item key included because value is truthy
 *   'class3' => false,  // Item key not included because value is falsey
 * ]));
 *
 * @param  AttributeValueCollection  $values
 */
function attributeValue(Collection $values): string
{
    return $values
        ->map(function ($value, $key) {
            // $key is integer: include string $value
            if (is_int($key)) {
                return (string) $value;
            }

            // $key is string: include $key based on truthiness value of $key
            return boolval($value) ? $key : null;
        })
        ->reject(function ($value) {
            return is_null($value);
        })
        ->sort()
        ->implode(' ')
    ;
}

/** @param AttributeCollection $attributes */
function attributeString(Collection $attributes): string
{
    foreach (['aria', 'data'] as $key) {
        if ($attributes->has($key)) {
            if (! is_array($attributes[$key])) {
                throw new \Exception("Invalid {$key} attribute type");
            }

            $keyAttributes = collect($attributes[$key])
                ->mapWithKeys(function ($value, $subkey) use ($key) {
                    return ["{$key}-{$subkey}" => $value];
                })
            ;

            $attributes = $attributes->merge($keyAttributes);

            $attributes->forget($key);
        }
    }

    return $attributes
        ->reject(function ($value) {
            if (is_instance_of($value, Collection::class)) {
                return $value->isEmpty();
            }

            return is_null($value) || $value === false || $value === '' || $value === [];
        })
        ->map(function ($value) {
            if (is_bool($value)) {
                return $value;
            }

            if (is_array($value)) {
                $value = collect($value);
            }

            if (is_instance_of($value, Collection::class)) {
                $value = attributeValue($value);
            }

            return htmlspecialchars((string) $value, ENT_QUOTES);
        })
        ->map(function ($value, $key) {
            return $value === true ? $key : "{$key}='{$value}'";
        })
        ->sort()
        ->implode(' ')
    ;
}

function makeId(string $name): string
{
    return str_replace(['[', '][', ']'], ['_', '_', ''], $name);
}

/** @param AttributeValue $value */
function makeCheckId(string $name, mixed $value): string
{
    if (is_array($value) || (is_object($value) && is_a($value, Collection::class))) {
        $value = is_array($value) ? collect($value) : $value;
        $value = attributeValue($value);
    }

    return makeId($name) . '_' . Str::snake((string) $value);
}

function oldValue(string $name, mixed $default = null): mixed
{
    $model = resolve(FormModelStack::class)->last();
    $attributeValue = null;
    if (is_instance_of($model, Model::class) && $model->hasAttribute($name)) {
        $attributeValue = $model->getAttribute($name);
    } elseif (is_object($model) && isset($model->{$name})) { // @phpstan-ignore property.dynamicName
        $attributeValue = $model->{$name}; // @phpstan-ignore property.dynamicName
    }

    // Ignore relationship attributes.
    if (is_object($attributeValue) && is_a($attributeValue, Model::class)) {
        $attributeValue = null;
    }

    // '1' and '0' are used to represent boolean values in HTML.
    if (is_bool($attributeValue)) {
        $attributeValue = $attributeValue ? '1' : '0';
    }

    return old(dotify($name), $attributeValue) ?? $default;
}

function dotify(string $text): string
{
    return (string) Str::of($text)->replaceMatches(
        '/\[([^]]+)\]/',
        fn($m) => ".{$m[1]}",
    );
}
