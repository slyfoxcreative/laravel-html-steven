<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

use function SlyFoxCreative\Html\Helpers\attributeValue;
use function SlyFoxCreative\Html\Helpers\dotify;
use function SlyFoxCreative\Html\Helpers\makeCheckId;
use function SlyFoxCreative\Html\Helpers\makeId;
use function SlyFoxCreative\Html\Helpers\oldValue;
use function SlyFoxCreative\Utilities\assert_string;
use function SlyFoxCreative\Utilities\is_instance_of;

/** @param AttributeList $attributes */
function label(
    string $id,
    ?string $text = null,
    array|Collection $attributes = [],
): HtmlString {
    $text ??= Str::ucfirst(str_replace(['_id', '_'], ['', ' '], $id));

    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    $attributes['for'] = $id;

    $attributes = resolve(AttributeModifierRegistry::class)->modify($attributes, 'label');

    return complete_tag('label', $text, $attributes);
}

/**
 * @phpstan-assert null|int|string $value
 */
function assert_selected_value(mixed $value): void
{
    if (!is_null($value) && !is_int($value) && !is_string($value)) {
        throw new \LogicException('Invalid selected value');
    }
}

/**
 * @param  OptgroupList  $options
 * @param  AttributeList  $attributes
 * */
function select(
    string $name,
    array|Collection $options,
    array|Collection $attributes = [],
): HtmlString {
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    $attributes['name'] = $name;
    $attributes['id'] ??= makeId($name);

    $selectedValue = $attributes['selected'] ?? oldValue($name);
    $attributes->forget('selected');
    assert_selected_value($selectedValue);

    $attributes = resolve(AttributeModifierRegistry::class)->modify($attributes, 'select', ['name' => dotify($name)]);

    if (is_array($options)) {
        $options = collect($options);
    }

    $tags = collect([
        opening_tag('select', $attributes),
    ]);

    $options = $options->map(function ($value, $key) use ($selectedValue) {
        if (is_array($value) || is_instance_of($value, Collection::class)) {
            return optgroup((string) $key, $value, [
                'selected' => $selectedValue,
            ]);
        }

        return option((string) $value, [
            'value' => $key,
            // Use stringwise comparison to ensure correct behavior even if the
            // input value is numeric but the flashed-input value is a string.
            'selected' => (string) $key === (string) $selectedValue,
        ]);
    });

    $tags = $tags->merge($options);

    $tags->push(closing_tag('select'));

    return new HtmlString($tags->map->toHtml()->implode("\n"));
}

/**
 * @param  OptionList  $options
 * @param  AttributeList  $attributes
 * */
function optgroup(
    string $label,
    array|Collection $options,
    array|Collection $attributes,
): HtmlString {
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    $selectedValue = $attributes['selected'] ?? null;
    $attributes->forget('selected');

    $options = is_array($options) ? collect($options) : $options;

    $tags = collect([
        opening_tag('optgroup', ['label' => $label]),
    ]);

    $tags = $tags->merge($options->map(function ($label, $value) use ($selectedValue) {
        return option((string) $label, [
            'value' => $value,
            'selected' => $value === $selectedValue,
        ]);
    }));

    $tags->push(closing_tag('optgroup'));

    return new HtmlString($tags->map->toHtml()->implode("\n"));
}

/** @param AttributeList $attributes */
function option(string $label, array|Collection $attributes): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    return complete_tag('option', $label, $attributes);
}

/** @param AttributeList $attributes */
function textarea(string $name, ?string $text = null, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    $attributes['name'] = $name;
    $attributes['id'] ??= makeId($name);

    $attributes = resolve(AttributeModifierRegistry::class)->modify($attributes, 'textarea', ['name' => dotify($name)]);

    $text ??= $attributes['value'] ?? null;
    $text ??= oldValue($name, '');
    $attributes->forget('value');
    assert_string($text);

    return complete_tag('textarea', $text, $attributes);
}

/** @param AttributeCollection $attributes */
function input(string $name, Collection $attributes): HtmlString
{
    $attributes['name'] = $name;
    $attributes['id'] ??= makeId($name);
    $attributes['value'] ??= oldValue($name);

    $attributes = resolve(AttributeModifierRegistry::class)->modify($attributes, 'input', ['name' => dotify($name)]);

    return opening_tag('input', $attributes);
}

/** @param AttributeList $attributes */
function checkbox(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'checkbox';

    if (! $attributes->has('id')) {
        $attributes['id'] = $attributes->has('value')
            ? makeCheckId($name, $attributes['value'])
            : makeId($name);
    }

    if (! $attributes->has('value')) {
        $attributes['value'] = '1';
    }

    if (! $attributes->has('checked')) {
        // Use stringwise comparison to ensure correct behavior even if the
        // input value is numeric but the flashed-input value is a string.
        $value = is_scalar($attributes['value'])
            ? (string) $attributes['value']
            : $attributes['value'];
        $oldValue = oldValue($name);
        if (is_scalar($oldValue)) {
            $oldValue = (string) $oldValue;
        }
        $attributes['checked'] = $oldValue === $value;
    }

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function date(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'date';
    $oldValue = oldValue($name);
    if (is_instance_of($oldValue, \DateTime::class)) {
        $oldValue = $oldValue->format('Y-m-d');
    }
    $attributes['value'] ??= $oldValue;

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function datetime_local(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'datetime-local';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function email(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'email';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function file(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'file';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function hidden(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'hidden';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function month(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'month';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function number(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'number';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function password(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'password';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function radio(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'radio';
    if (! $attributes->has('checked')) {
        // Use stringwise comparison to ensure correct behavior even if the
        // input value is numeric but the flashed-input value is a string.
        $value = $attributes['value'] ?? 'on';
        if (is_scalar($value)) {
            $value = (string) $value;
        }
        $oldValue = oldValue($name);
        if (is_scalar($oldValue)) {
            $oldValue = (string) $oldValue;
        }
        $attributes['checked'] = $oldValue === $value;
    }

    if (! $attributes->has('id')) {
        $attributes['id'] = $attributes->has('value')
            ? makeCheckId($name, $attributes['value'])
            : makeId($name);
    }

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function range(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'range';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function reset(string $label, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'reset';
    $attributes['value'] = $label;

    if ($attributes->has('name')) {
        $name = $attributes['name'];
        if (is_array($name) || (is_object($name) && is_a($name, Collection::class))) {
            $name = is_array($name) ? collect($name) : $name;
            $name = attributeValue($name);
        }
        $name = (string) $name;
        $attributes->forget('name');
    } else {
        $name = '';
    }

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function search(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'search';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function submit(string $label, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'submit';
    $attributes['value'] = $label;

    if ($attributes->has('name')) {
        $name = $attributes['name'];
        if (is_array($name) || (is_object($name) && is_a($name, Collection::class))) {
            $name = is_array($name) ? collect($name) : $name;
            $name = attributeValue($name);
        }
        $name = (string) $name;
        $attributes->forget('name');
    } else {
        $name = '';
    }

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function tel(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'tel';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function text(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'text';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function time(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'time';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function url(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'url';

    return input($name, $attributes);
}

/** @param AttributeList $attributes */
function week(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;
    $attributes['type'] = 'week';

    return input($name, $attributes);
}
